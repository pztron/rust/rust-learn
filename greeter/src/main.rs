use std::io;


fn main() {
    println!("Nice to meet you, what's your name?");
    loop {
        let mut input = String::new();
        let len = io::stdin().read_line(&mut input).unwrap_or_else(|error| {
            panic!("Problem reading input: {}", error);
        });
        if len == 1 {
            println!("Didn't catch that?");
        } else {
            print!("Hi there, {}", input);
            break;
        }

    }
    
    
}