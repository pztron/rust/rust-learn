use std::fmt;

struct Rectangle {
    width: u32,
    height: u32,
}

// make it such that rect1 can be printed with println!("{}", rect1)
impl fmt::Display for Rectangle {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{} px by {} px", self.width, self.height)
    }
}

// some methods and an associated function
impl Rectangle {
    fn area(&self) -> u32 {
        self.width * self.height
    }

    fn can_hold(&self, other: &Rectangle) -> bool {
        self.width > other.width && self.height > other.height
    }

    // associated functions are often useful when you want to programatically impliment and return a struct
    fn square(size: u32) -> Rectangle {
        Rectangle { width: size, height: size }
    }
}

fn main() {
    let rect1 = Rectangle { width: 30, height: 50 };
    println!("rect1 is {}", rect1);
    println!(
        "The area of the rectangle is {} square pixels",
        rect1.area()
    );
    let rect2 = Rectangle { width: 10, height: 40 };
    let rect3 = Rectangle { width: 60, height: 45 };

    println!("Can rect1 hold rect2? {}", rect1.can_hold(&rect2));
    println!("Can rect1 hold rect3? {}", rect1.can_hold(&rect3));

    // associated functions are called with different syntax as well
    let sq1 = Rectangle::square(3);
    println!("sq1 is {}", sq1);
    
}