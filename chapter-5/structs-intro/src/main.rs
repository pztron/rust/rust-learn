// defining the "user struct"
struct User {
    username: String,
    email: String,
    sign_in_count: u64,
    active: bool,

}

fn main() {
    let mut user1 = User {
        email: String::from("someone@example.com"),
        username: String::from("someusername123"),
        active: true,
        sign_in_count: 1,
    };

    // sincethe struct is mutable we can modify email
    user1.email = String::from("anotheremail@example.com");

    // simple function to create a new user
    let user2 = build_user(
        String::from("someone@example.com"),
        String::from("someusername123"),
    );

    // we can also use previous values to update a struct
    let user3 = User {
        email: String::from("another@example.com"),
        username: String::from("anotherusername567"),
        ..user2
    };
    // tuple structs allow you to create new types while still behaving like tuples
    struct Color(i32, i32, i32);
    struct Point(i32, i32, i32);

    let black = Color(0, 0, 0);
    let origin = Point(0, 0, 0);

}

fn build_user(email: String, username: String) -> User {
    User {
        email, // since the field and parameter have the same name we can use this shorthand
        username,
        active: true,
        sign_in_count: 1,
    }
}

fn main() {

}



