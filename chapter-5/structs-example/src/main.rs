use std::fmt;

struct Rectangle {
    width: u32,
    height: u32,
}

impl fmt::Display for Rectangle {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{} px by {} px", self.width, self.height)
    }
}
fn main() {
    let rect1 = Rectangle { width: 30, height: 50 };
    println!("rect1 is {}", rect1);
    println!(
        "The area of the rectangle is {} square pixels",
        area(&rect1)
    );
}


fn area(rectangle: &Rectangle) -> u32 {
    rectangle.width * rectangle.height
}