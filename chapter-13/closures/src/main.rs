use std::thread;
use std::time::Duration;

fn simulated_expensive_calculation(intensity: u32) -> u32 {
    println!("calculating slowly...");
    thread::sleep(Duration::from_secs(2));
    intensity
}

fn generate_workout_og(intensity: u32, random_number: u32) {
    if intensity < 25 {
        println!(
            "Today, do {} pushups!",
            // we have to call the expensive calculation multiple times which makes the program really slow
            simulated_expensive_calculation(intensity)
        );
        println!(
            "Next, do {} situps!",
            simulated_expensive_calculation(intensity)
        );
    } else {
        if random_number == 3 {
            println!("Take a break today! Remember to stay hydrated!");
        } else {
            println!(
                "Today, run for {} minutes!",
                simulated_expensive_calculation(intensity)
            );
        }
    }
}


fn generate_workout_slow(intensity: u32, random_number: u32) {
    // this calls the function no matter what, so we have to wait even if we don't call the expensive calculation (eg intensity is < 15 and random_number = 3)
    let expensive_result = simulated_expensive_calculation(intensity);

    if intensity < 25 {
        println!("Today, do {} pushups!", expensive_result);
        println!("Next, do {} situps!", expensive_result);
    } else {
        if random_number == 3 {
            println!("Take a break today! Remember to stay hydrated!");
        } else {
            println!("Today, run for {} minutes!", expensive_result);
        }
    }
}

struct Cacher<T>
where
// specify a generic type which specifies that it's a closure by using Fn() and must take and return an u32
    T: Fn(u32) -> u32,
{
    // make fields private because our methods handle everything better anyway
    calculation: T,
    // want to use an Option because it is either None (before execution) or Some(u32)
    value: Option<u32>,
}

impl<T> Cacher<T>
where
    // specify the generic T
    // Fn() means borrow immutably
    // FnMut() means mutably borrow
    // FnOnce() takes ownership
    T: Fn(u32) -> u32,
{
    // defines Cacher::new takes a Calculation which fits the specifications of T and returns a Cacher struct
    fn new(calculation: T) -> Cacher<T> {
        Cacher {
            calculation,
            value: None,
        }
    }
    // this cacher is still flawed because once it caches a value it can never changle ie Cacher.value(1); \n Cacher.value(2); will both return 1 because 1 has been cached
    // the cacher should only return the same value if the same argument is used twice
    fn value(&mut self, arg: u32) -> u32 {
        match self.value {
            // if there is Some(v) then return it
            Some(v) => v,
            // is there is None(v) then calculate v and return v
            None => {
                let v = (self.calculation)(arg);
                self.value = Some(v);
                v
            }
        }
    }
}

fn generate_workout_fast(intensity: u32, random_number: u32) {
    // closures don't require you to annotate types (however you can with |num: u32| -> u32) because they're inferred
    // because types are inferred a closure cannot be called twice with different types
    // closures can also acess all the variables in their scope
    // to take ownership of the variables put "move" before the arguments
    let mut expensive_result = Cacher::new(|num| {
        println!("calculating slowly...");
        thread::sleep(Duration::from_secs(2));
        num
    });

    if intensity < 25 {
        println!("Today, do {} pushups!", expensive_result.value(intensity));
        println!("Next, do {} situps!", expensive_result.value(intensity));
    } else {
        if random_number == 3 {
            println!("Take a break today! Remember to stay hydrated!");
        } else {
            println!(
                "Today, run for {} minutes!",
                expensive_result.value(intensity)
            );
        }
    }
}

fn main() {
    let simulated_user_specified_value = 10;
    let simulated_random_number = 7;

    generate_workout_og(simulated_user_specified_value, simulated_random_number);
    generate_workout_slow(simulated_user_specified_value, simulated_random_number);
    generate_workout_fast(simulated_user_specified_value, simulated_random_number);
}
