fn main() {
    let v1 = vec![1, 2, 3];
    // iterators are lazy meaning that just calling an iterator doesn't do anything until you use a for loop
    let v1_iter = v1.iter();

    // now the iterator does something
    for val in v1_iter {
        println!("Got: {}", val);
    }

    // iterator adapters convert iterators into different types of iterators
    let v1: Vec<i32> = vec![1, 2, 3];

    // this closure takes each value of v1's iterator and adds one to it, however, it doesn't do anything until called because iterators are lazy
    v1.iter().map(|x| x + 1);

    let v2: Vec<_> = v1.iter().map(|x| x + 1).collect(); // calling collect collects all of the values from the iterator into a vector
    assert_eq!(v2, vec![2, 3, 4]);


    // you can create your own vector becuase all the Iterator trait requires iss an implementation of "next"
    struct Counter {
        count: u32,
    }
    
    impl Counter {
        fn new() -> Counter {
            Counter { count: 0 }
        }
    }

    impl Iterator for Counter {
        type Item = u32;
    
        fn next(&mut self) -> Option<Self::Item> {
            if self.count < 5 {
                self.count += 1;
                Some(self.count)
            } else {
                None
            }
        }
    }
    let sum: u32 = Counter::new() // make a new counter
        .zip(Counter::new().skip(1)) // change the sum in a vector tuples with the the first value being this counter and the second value being a different counter pushed by 1
        .map(|(a, b)| a * b) // multiple the tuples
        .filter(|x| x % 3 == 0) // only keep those divisible by 3
        .sum(); // sum
    

}
