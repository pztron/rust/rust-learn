fn main() {
    // there are two main types of string in rust - the heap allocated String and the stack allocated str
    // they are both UTF-8 encoded, however only String is growable



    // new string
    let mut s = String::new();

    let data = "intial contents";

    // three ways to create a String from a str
    let s = data.to_string(); // convert str (string literal) into a String
    let s = "intial contents".to_string();
    let s = String::from("intial contents");

    // appending to a string with push_str and push
    let mut s = String::from("foo");
    s.push_str("bar")

    let mut s1 = String::from("foo");
    let s2 = "bar";
    s1.push_str(s2); // push_str takes a str so ownership isn't taken and you can't use s2 later
    println!("s2 is {}", s2);

    let mut s = String::from("lo");
    s.push('l'); // .push takes a single character like putchar from C

    // string concatenation
    let s1 = String::from("Hello, ");
    let s2 = String::from("world!");
    let s3 = s1 + &s2; // note s1 has been moved here and can no longer be used

    let s1 = String::from("tic");
    let s2 = String::from("tac");
    let s3 = String::from("toe");

    let s = format!("{}-{}-{}", s1, s2, s3);

    // this is how you slice into strings as each character is two bytes (a unicode scalar)
    let hello = "Здравствуйте";

    let s = &hello[0..4];

    // you can either call the raw unicode scalars (this may give unexpected nbehavior) or the raw bytes - getting grapheme clusters is more complex
    for c in "नमस्ते".chars() {
        println!("{}", c);
    }
    for b in "नमस्ते".bytes() {
        println!("{}", b);
    }


}
