fn main() {
    

    use std::collections::HashMap; // hash maps are not automatically brought into scope, nor do they have a macro for declaring and constructing tem

    // hash maps are like vectors, the type of the all the keys and all of the values must be the same and it's stored on the heap
    let mut scores = HashMap::new();

    scores.insert(String::from("Blue"), 10);
    scores.insert(String::from("Yellow"), 50);

    let teams = vec![String::from("Blue"), String::from("Yellow")];
    let initial_scores = vec![10, 50];

    let team_name = String::from("Blue");
    let score = scores.get(&team_name); // you can get values using a key in the get method

    for (key, value) in &scores { // a hash map can be interated over as well
        println!("{}: {}", key, value);
    }

    let mut scores: HashMap<_, _> = // _, _ is used as types are uinkown
        teams.into_iter().zip(initial_scores.into_iter()).collect(); // .zio() creates a vector of tuples and .collect gathers data into collections like hash maps

    
    let field_name = String::from("Favorite color");
    let field_value = String::from("Blue");
    
    let mut map = HashMap::new();
    map.insert(field_name, field_value); // Strings cannot be copied so they're moved an can no longer be used


    let mut scores = HashMap::new();
    scores.insert(String::from("Blue"), 10);

    scores.entry(String::from("Yellow")).or_insert(50); // if you call two .insert() s  the value will be overwritten, this writes only if the key doesn't exist
    scores.entry(String::from("Blue")).or_insert(50);

    println!("{:?}", scores);




    // this codes counts the number of each word
    let text = "hello world wonderful world";

    let mut map = HashMap::new();

    for word in text.split_whitespace() {
        let count = map.entry(word).or_insert(0);
        *count += 1; // derefernce so we actually modify the hashmap
    }

    println!("{:?}", map);
}
