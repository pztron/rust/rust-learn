fn main() {
    // vectors are allocated on the heap as the size isn't known at compile time, and are of only one type
    let v = Vec<i32> = Vec::new();
    // above is a new vector of i32s

    // rust can also infer the type
    let v = vec![1, 2, 3];

    // here's how you update a vector
    let mut v = Vec::new();

    v.push(5);
    v.push(6);
    v.push(7);
    v.push(8);

    // the two ways to acess elements of a vector
    let v = vec![1, 2, 3, 4, 5];

    
    let third: &i32 = &v[2]; // use a reference so v[2] isn't copied out of the vector
    println!("The third element is {}", third);

    match v.get(2) {
        Some(third) => println!("The third element is {}", third),
        None => println!("There is no third element."),
    }


    // this will throw an error since there's both a mutable and immutable reference
    let mut v = vec![1, 2, 3, 4, 5];

    let first = &v[0]; // immutable reference

    v.push(6); // mutable

    println!("The first element is: {}", first);


    // iterating over a vector is as expected
    let mut v = vec![100, 32, 57];
    for i in &mut v {
        *i += 50;
    }



    // holding multiple types in a vector using an enum as everything in an enum is a variatiant of the enum's type
    enum SpreadsheetCell {
        Int(i32),
        Float(f64),
        Text(String),
    }

    let row = vec![
        SpreadsheetCell::Int(3),
        SpreadsheetCell::Text(String::from("blue")),
        SpreadsheetCell::Float(10.12),
    ];
} // v goes out of scope here just like any other struct
