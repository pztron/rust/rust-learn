fn main() {

    out_of_range();
    // rust prints failure message, cleans up memory, then quits
    panic!("F");
}

fn out_of_range() {
    let v = vec![1, 2, 3];
    // this sort of thing is really dangerous in C because it'll just go to the memory location where v[99] should be
    // so a hacker could manipulate this really easily.
    // this also demonstrates how rust shows you where vector is implemented in rust rather than what code caused the error, so you need a backtrace
    v[99];
}
