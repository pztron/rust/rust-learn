use std::fs::File;
use std::io::ErrorKind;
use std::io::Read;

fn main() {
    // returns an enum Result<T,E>
    let f = File::open(
        "hello.txt"
    );
    // using a basic match stsatement to debug (Closures are better though)
    let f = match f {
        // Result::Ok is already in scope so we don't have to specify
        Ok(file) => file,
        // nested match to handle if the file isn't found
        Err(error) => match error.kind() {
            ErrorKind::NotFound => match File::create("hello.txt") {
                Ok(fc) => fc,
                Err(e) => panic!("Problem creating the file: {:?}", e),
            },
            other_error => {
                panic!("Problem opening the file: {:?}", other_error)
            }
        },
    };

    let f = File::open("hello.txt").unwrap(); // unwrap returns the file inside of Ok or panics (just like the match statement)
    let f = File::open("hello.txt").expect("Failed to open hello.txt"); // passes this string to the panic in unwrap
    read_username_from_file();
}


fn read_username_from_file() -> Result<String, io::Error> {
    let mut f = File::open("hello.txt")?; // putting a ? returns Ok if okay to the variable, and Err to the entire function if there's an error (it also converts the error to the reutnr type)
    let mut s = String::new();
    f.read_to_string(&mut s)?;
    Ok(s)
}

