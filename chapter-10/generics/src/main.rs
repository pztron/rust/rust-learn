// takes a slice of some type T and returns the same type T (still needs a trait as it won't work for all type)
fn largest<T>(list: &[T]) -> T {
    let mut largest = list[0];

    for &item in list {
        if item > largest {
            largest = item;
        }
    }

    largest
}
// struct creates Point<T> that'll take any type T
struct Point<T> {
    x: T,
    y: T,
}
// methods on generic structs
impl<T> Point<T> {
    fn x(&self) -> &T {
        &self.x
    }
}
// we can also implement methods only when T is a certain type
impl Point<f32> {
    fn distance_from_origin(&self) -> f32 {
        (self.x.powi(2) + self.y.powi(2)).sqrt()
    }
}

fn main() {
    let number_list = vec![34, 50, 25, 100, 65];

    let result = largest(&number_list);
    println!("The largest number is {}", result);

    let char_list = vec!['y', 'm', 'a', 'q'];

    let result = largest(&char_list);
    println!("The largest char is {}", result);

    let integer = Point { x: 5, y: 10 };
    let float = Point { x: 1.0, y: 4.0 };
    // must be of same type T
    let wont_work = Point { x: 5, y: 4.0 };
}
