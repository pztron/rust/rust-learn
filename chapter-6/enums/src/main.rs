
// any kind of data can be in an enum variant (structs or even another enum)
// ipv4 and ipv6 are both ip addresses but in different formats so we enumerate them along with their data type
enum IpAddr {
    V4(u8, u8, u8, u8),
    V6(String),
}

let home = IpAddr::V4(127, 0, 0, 1);

let loopback = IpAddr::V6(String::from("::1"));

// here's how the standard library handles ip addresses
/*
struct Ipv4Addr {
    // --snip--
}

struct Ipv6Addr {
    // --snip--
}

enum IpAddr {
    V4(Ipv4Addr),
    V6(Ipv6Addr),
}
*/

// here's another enum (Move has an anonymus struct in it and write and changecolor have tuples)
enum Message {
    Quit,
    Move { x: i32, y: i32 },
    Write(String),
    ChangeColor(i32, i32, i32),
}

impl Message {
    fn call(&self) {
        // making methods is the same as well
    }
}


fn main() {
    // the variants are namespaced under the enum's identifier and use a double-colon to sperare
    let four = IpAddrKind::V4;
    let six = IpAddrKind::V6;

    // we can call it with either variant
    route(IpAddrKind::V4);
    route(IpAddrKind::V6);
    
    // we declare m a that is of kind Write and value "hello"
    let m = Message::Write(String::from("hello"));
    m.call();

    /*
    Rust doesn't have null it has the option enum, which you don't even need to bring into scope with Option::
    enum Option<T> {
        Some(T),
        None,
    }
    this allows rust to only have to check for null when using enum Option and not with normal types
    */
    let some_number = Some(5);
    let some_string = Some("a string");
    let absent_number: Option<i32> = None;
    
}

// we can now define a function that takes any ipaddrkind
fn route(ip_kind: IpAddrKind) {}
