fn main() {
    let some_u8_value = Some(0u8);
    // if let allows code to only be executed on a certain kind or value meaning there doesn't always have to be a need for "_ => (),"
    if let Some(3) = some_u8_value {
        println!("three");
    }
}
