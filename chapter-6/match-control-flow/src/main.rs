
// simple enum with types of coin
enum Coin {
    Penny,
    Nickel,
    Dime,
    Quarter(UsState),
}
#[derive(Debug)]
enum UsState {
    Alabama,
    Alaska,
    // other states here
}
fn value_in_cents(coin: Coin) -> u8 {
    // depending on the enum's kind it returns different values
    match coin {
        Coin::Penny => {
            println!("Lucky penny!");
            1
        }
        Coin::Nickel => 5,
        Coin::Dime => 10,
        // variable state points to the enum UsState
        Coin::Quarter(state) => {
            println!("State quarter from {:?}!", state);
            25
        }
    }
}

// you must cover all cases in rust however "_ => ()," covers all uncovered cases
fn plus_one(x: Option<i32>) -> Option<i32> {
    match x {
        None => None,
        Some(i) => Some(i + 1),
    }
}

fn main() {
    let five = Some(5);
    // you can use words for numbers
    let six = plus_one(five);
    let none = plus_one(None);

}
