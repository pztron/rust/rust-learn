use std::env;

fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() == 3 {
        println!("Hello {}", args[2])
    } else {
        println!("Hello there!")
    }
    
}
