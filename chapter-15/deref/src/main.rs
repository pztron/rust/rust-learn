struct MyBox<T>(T); // our new smart pointer which is a tuple struct

impl<T> MyBox<T> {
    fn new(x: T) -> MyBox<T> {
        MyBox(x)
    }
}

impl<T> Deref for MyBox<T> {
    type Target = T; // associated type

    fn deref(&self) -> &T {
        &self.0
    }
}

fn main() {
    let x = 5;
    let y = MyBox::new(x);


    assert_eq!(5, x);
    assert_eq!(5, y); // doesn't work as it is comparing an actual 5 to a reference to a 5
    assert_eq!(&5, y); // works
    assert_eq!(5, *y); // also works, th9ugh we have to implement a deref for it to work

    let m = MyBox::new(String::from("Rust")); // returns a &MyBox<String>
    hello(&m); // rust automatically uses the deref we implemented to get a String, then slices a &String to give us the &str we need

}

fn hello(name: &str) {
    println!("Hello, {}!", name);
}
