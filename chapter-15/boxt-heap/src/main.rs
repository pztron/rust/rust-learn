enum List { 
    Cons(i32, Box<List>), // list must be "boxed" so that rust knows exactly how large this enum could be when creates
    Nil,
}

use crate::List::{Cons, Nil};

fn main() {
    /*
    Boxes store a pointer to data on the heap on the stack, they're usually used for:
        * When you don't know a types size at compile time but you want to reference the exact size
        * You have a large amount of data and want to transfer ownership but not copy the data
        * When you own a value and only care that it implements a certain trait
    */
    let b = Box::new(5); // i32 5 is stored on the heap using a box and goes at of scope at the end of the block
    println!("b = {}", b)

    // boxes are useful for recursive types wherein a type contain that same type
    let list = Cons(1, Box::new(Cons(2, Box::new(Cons(3, Box::new(Nil))))));
}
