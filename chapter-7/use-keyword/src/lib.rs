mod front_of_house {
    pub mod hosting {
        pub fn add_to_waitlist() {}
    }
}

// effectivley a symbolic link using an absolute path
// always bring the parent module into scope (for functions), not just the thing you want
// "pub use" will re-export code putting under the scope of where use is defined
// you can bring multiple things from the same path using "use std::{thing1, thing2};"
use crate::front_of_house::hosting;

pub fn eat_at_restaurant() {
    hosting::add_to_waitlist();
    hosting::add_to_waitlist();
    hosting::add_to_waitlist();
}

fn main() {}

