fn main() {
    println!("The value of z is {}", multiply(5, 6));
}
fn multiply(x: i32, y: i32) -> i32 { // don't name return value
    println!("The value of x is: {}", x);
    println!("The value of y is: {}", y);
    x*y // return value is usually implicit (the last expression), if you need to return early, do it explicitly ie return x*yc 
}
