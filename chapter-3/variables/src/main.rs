fn main() {
    let mut x = 5; // add a "mut" after let to make a variable mutable
    println!("The value of x is: {}", x);
    x = 6;
    println!("The value of x is: {}", x);
// Consts
// * Always immutable
// * Use uppercase and underscores
// * Can use _ in ints
// * Can be used globally
// * Valid for all of runtime
    const MAX_POINTS: u32 = 100_100; 
// Shadowing a variable - different from mut (creating new variable with same name)
    let x = 5;
    let x = x + 1;
    let x = x * 2;
    println!("The value of x is: {}", x);
// Shadowing can be useful for changing type as mut only chnages value not type
    let spaces = "   ";
    let spaces = spaces.len();
// If type cannot be inferred it must be specified, even with let
    let guess: u32 = "42".parse().expect("Not a number");
// Characters are  4 byte unicode scalar value with single quotes
    let smile = '😃';
// Tuples put different types in an inmutable array
    let tup: (i32, f64, u8) = (-500, 6.4, 1);
// Multiple assignment like this destroys (destructures) tuples
    let (x, y, z) = tup;
    println!("The value of y is: {}", y);
// You can also just refer directly to the values in a tuple with destrucuring it
    let x: (i32, f64, u8) = (-500, 6.4, 1);
    let negative_five_hundreed = x.0;
    let six_point_four = x.1;
    let one = x.2;
// An array is as expected except they put data on the stack (one chunk of memory) rather than the heap and they can't grow or shrink
    let a = [1, 2, 3, 4, 5];
    let fifth = a[4]; // acessing elements - be careful the compiler only checks if your element is in range on runtime not compile
    let a: [i32; 5]  = [1, 2, 3, 4, 5]; // you don't have to assign type but you can
    let a = [3; 5] // fills the array with 3 fives
// expression vs statement

    let y = {
        let x = 3;  // statements do and don't return value
        x + 1 // expressions don't have semicolons and return values, adding a semicolon means it will not longer return 4
    }
}

// function declaration

fn another_function() {
    println!("Another function!");
}
