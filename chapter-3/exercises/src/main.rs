fn main() {
    let temps = [0.0, 32.0, 100.0, 212.0];
    convert(&temps);
    // below is the largest fib my quick and dirty function can calculate as I'm not implementing 256 bit integers
    println!(" {}", fibb(186));
    christmas()
}

fn convert(temps: &[f64]) {
    for temp in temps.iter() {
        println!("{}° F is {}° C", temp, (temp - 32.0) * 5.0/9.0);
    }

}


fn fibb(nth: u128) -> u128 {
    let mut fibb: Vec<u128> = vec![0, 1];
    for index in 1..nth {
        let newfibb = fibb[((index) as usize)] + fibb[((index -1) as usize)];
        fibb.push(newfibb);
        if index == (nth - 1) {
            println!("The {}th fibbonachi number is", nth);
            return fibb[fibb.len()-1]
            
        }
    }
    0
}

fn christmas() {
    let times = ["first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eight", "ninth", "tenth", "eleventh", "twelfth"];
    let things = ["A partridge in a pear tree", "Two turtle doves", "Three French hens", "Four calling birds", "Five gold rings", "Six geese a laying", "Seven swans a swimming", "Eight maids a milking", "Nine ladies dancing", "Ten lords a leaping", "Eleven pipers piping", "12 drummers drumming"];

    for (ti, tu) in times.iter().zip(things.iter().enumerate()) {
        println!("On the {} day of christmas \nMy true love gave to me", ti);
        for num in (1..(tu.0 + 1)).rev() {
            println!("{}", things[num]);
            
        }
        if tu.0 == 0 {
            println!("{}",tu.1)
        } else {
            println!("And a partridge in a pear tree");
        }

    }

}
