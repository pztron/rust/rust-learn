fn main() {
    let num = 7;
    if num < 5 { // there are no truthy or falsey values
       println!("The condition was true");
    } else {
       println!("The condition was false");
    }
    // since if is a statement we can use it in let
    let condition = true
    let number = if condition {
        5 // if you use expressions they must evaluate to the same type no 5 and "six"
    } else {
        6
    };

}

fn loops() {
// "loop" is just a while (true) loop with the normal things
    let mut counter = 0;
    let result = loop {
        counter += 1;
        if counter == 10 {
            break counter * 2;
        }
    };
// while works as expected
    let a = [10, 20, 30, 40, 50];
    let mut index = 0;
    while index < 5 { // very slow
        println!("The value is: {}", a[index]);
        index += 1
    }
// for loops
    for element in a.inter() { //fast
        println!("The value is: {}", element)
    }
// for loops are more safe so there are methods to replace things where you'd use while loops
    for number in (1..5).rev() { // .rev does things in reverse, and this acts as range
        println!("{}!", number);
    }
    println!("LIFTOFF")
}