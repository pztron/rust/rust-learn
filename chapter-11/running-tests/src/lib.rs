/*
running "cargo test -- --test-threads=1" insures that the tests run consequently instead of in parallel
cargo test -- --show-output shows output even when a test suceeds
cargo test [function name] will only run that test
cargo test [string in multiple function names] will run tests with that string in their name
#[ignore] will ignore the test unless specifically called
cargo test -- --ignored runs all tests with the ignore attribute


*/
fn prints_and_returns_10(a: i32) -> i32 {
    println!("I got the value {}", a);
    10
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn this_test_will_pass() {
        let value = prints_and_returns_10(4); // values printed to stdout don't get printed to stdout when the test suceeds
        assert_eq!(10, value);
    }

    #[test]
    fn this_test_will_fail() {
        let value = prints_and_returns_10(8);
        assert_eq!(5, value);
    }
}

