fn main() {
    scope();
    type_string();
    memory_pt2();
    ownership_functions();
    returnvalues_scope();
}



fn scope() {
    // s not valid
    let s = "hello"; // s (string literal) now valid
} // scope over no longer valid

fn type_string() {

    let mut s = String::from("hello"); // a string type allocated on the heap rather than the stack
} // memory is auto de-allocated by the rust drop function when it falls out of scope

fn memory_pt2() {
    let x = 5; // this is a copy type so the data in x is still usable after being bound to y
    let y = x; // binds 5 to x, makes a copy of x's value and binds it to y

    let s1 = String::from("hello");
    let s2 = s1 // instead of copying x's value from the heap, the pointer to s1 is copied to s2
                // they point to the same place on the heap
                // they have their "index" on the stack which contains the pointer to the data on the heap,
                // the length of the variable (in bytes), and its capacity (total number of bytes allocated)
                // however, when you allocate s2, s1's "index" is destroyed because if not rust will try to deallocate the same
                // place in memory twice (double free error) which causes memory safety issues
    
    let s1 = String::from("hello") 
    // if you really need both s1 and s2 as data on the heap, here's the memory safe way that actually
    let s2 = s1.clone(); // just copies the data on the heap to prevent double freeing (it is memory expensive though)


}

fn ownership_functions() {
    let s = String::from("hello");  // s comes into scope

    takes_ownership(s);             // s's value moves into the function...
                                    // ... and so is no longer valid here

    let x = 5;                      // x comes into scope

    makes_copy(x);                  // x would move into the function,
                                    // but i32 is Copy, so it’s okay to still
                                    // use x afterward

} // Here, x goes out of scope, then s. But because s's value was moved, nothing
  // special happens.

fn takes_ownership(some_string: String) { // some_string comes into scope
    println!("{}", some_string);
} // Here, some_string goes out of scope and `drop` is called. The backing
  // memory is freed.

fn makes_copy(some_integer: i32) { // some_integer comes into scope
    println!("{}", some_integer);
} // Here, some_integer goes out of scope. Nothing special happens.



fn returnvalues_scope() {
    let s1 = gives_ownership();         // gives_ownership moves its return
                                        // value into s1

    let s2 = String::from("hello");     // s2 comes into scope

    let s3 = takes_and_gives_back(s2);  // s2 is moved into
                                        // takes_and_gives_back, which also
                                        // moves its return value into s3
} // Here, s3 goes out of scope and is dropped. s2 goes out of scope but was
  // moved, so nothing happens. s1 goes out of scope and is dropped.

fn gives_ownership() -> String {             // gives_ownership will move its
                                             // return value into the function
                                             // that calls it

    let some_string = String::from("hello"); // some_string comes into scope

    some_string                              // some_string is returned and
                                             // moves out to the calling
                                             // function
}

// takes_and_gives_back will take a String and return one
fn takes_and_gives_back(a_string: String) -> String { // a_string comes into
                                                      // scope

    a_string  // a_string is returned and moves out to the calling function
}
